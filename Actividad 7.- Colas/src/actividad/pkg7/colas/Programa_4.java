package actividad.pkg7.colas;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Programa_4{
public static void main(String[] args) {
      int winner = joseph(40, 7);
		System.out.println("Vive el " + winner);
		winner = joseph(40, 7);
		
	}
	
	public static int joseph(int noOfPeople, int remPosition) {
		int tempPos = remPosition - 1;
		int[] people = new int[noOfPeople];
		
		for (int i = 0; i < noOfPeople; i++) {
			people[i] = i + 1;
		}
		
		int iteration = noOfPeople - 1;
		
		List<Integer> list = IntStream.of(people).boxed().collect(Collectors.toList());
		
		while (iteration > 0) {
			list.remove(tempPos);
			tempPos += remPosition - 1;
			if (tempPos > list.size() - 1) {
				tempPos = tempPos % list.size();
			}
			iteration--;
		}
		
		return list.get(0);
	}
	
}
